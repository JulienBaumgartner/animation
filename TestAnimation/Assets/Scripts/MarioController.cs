﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarioController : MonoBehaviour
{
    public enum State { Dead, Small, Big, Fire, Leaf, Frog};
    private State currentState = State.Small;

    [SerializeField]
    private float speedFactor;
    [SerializeField]
    private float jumpForce;

    private Rigidbody2D rigidBody;
    private Animator animator;
    private SpriteRenderer spriteRenderer;
    private float transitionStart = 0f;
    private bool doTransition = false;

    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if(currentState == State.Dead)
        {
            return;
        }
        if (doTransition)
        {
            Time.timeScale = 0;
            spriteRenderer.enabled = Time.realtimeSinceStartup % 0.1f< 0.05f;
            if(transitionStart+0.5f <= Time.realtimeSinceStartup)
            {
                doTransition = false;
                spriteRenderer.enabled = true;
                Time.timeScale = 1;
            }
        }
        bool isGrounded = Physics2D.Raycast(transform.position, Vector2.down, 0.03f, 256);
        float axisX = Input.GetAxis("Horizontal");
        float axisY = Input.GetAxisRaw("Vertical");
        float speedX = axisX * speedFactor;
        if(axisY < -0.05f && isGrounded)
        {
            rigidBody.velocity = new Vector2(0, rigidBody.velocity.y);
            animator.SetBool("isCrouched", true);
        }
        else
        {
            animator.SetBool("isCrouched", false);
            rigidBody.velocity = new Vector2(speedX, rigidBody.velocity.y);
            if (Mathf.Abs(rigidBody.velocity.x) > 0.05)
            {
                spriteRenderer.flipX = rigidBody.velocity.x < 0;
            }
        }

        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            rigidBody.AddForce(new Vector2(0, jumpForce));
        }
        animator.SetFloat("speed", Mathf.Abs(rigidBody.velocity.x));
        animator.SetBool("isJumping", !isGrounded);
    }

    void StateBehaviour()
    {
        switch (currentState)
        {
            case State.Dead:
                break;
            case State.Small:
                break;
            case State.Big:
                break;
            case State.Fire:
                break;
            case State.Leaf:
                break;
            case State.Frog:
                break;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.GetComponent<PowerUp>() != null)
        {
            ChangeState(collision.GetComponent<PowerUp>().behaviour);
            Destroy(collision.gameObject);
        }
    }

    void ChangeState(State newState)
    {
        if(currentState == newState)
        {
            return;
        }
        if(newState == State.Big && currentState != State.Small)
        {
            return;
        }
        currentState = newState;
        switch (newState)
        {
            case State.Dead:
                animator.SetBool("isDead", true);
                break;
            case State.Small:
                animator.SetTrigger("triggerSmall");
                break;
            case State.Big:
                animator.SetTrigger("triggerBig");
                break;
            case State.Fire:
                animator.SetTrigger("triggerFire");
                break;
            case State.Leaf:
                animator.SetTrigger("triggerLeaf");
                break;
            case State.Frog:
                animator.SetTrigger("triggerFrog");
                break;
        }
        doTransition = true;
        transitionStart = Time.realtimeSinceStartup;
    }
}
